from django.contrib.auth import get_user_model
from rest_framework import serializers

from .models import Category
from apps.files.serializers import FileSerializer


class CategorySerializer(serializers.ModelSerializer):
    """
    Serializer which outputs user data.
    """
    file = FileSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ['category_id', 'file', 'name', 'color']
        read_only_fields = ['category_id']
