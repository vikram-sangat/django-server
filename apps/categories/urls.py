from django.urls import path, re_path, include
from . import views
from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'', views.CategoryViewMode, basename='categories')

urlpatterns = [
    path('', include(router.urls), name='categories')
]
