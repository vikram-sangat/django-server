from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.parsers import MultiPartParser
from .serializers import FileSerializer, FileCreateSerializer
from .models import File

# Create your views here.


class FileViewModel(ReadOnlyModelViewSet):
    serializer_class = FileSerializer
    queryset = File.objects.all()
    lookup_url_kwarg = 'file_id'


class UploadFile(CreateAPIView):
    parser_classes = (MultiPartParser,)
    serializer_class = FileCreateSerializer
    permission_classes = (AllowAny,)
    authentication_classes = ()
