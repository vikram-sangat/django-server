from rest_framework import serializers

from .models import File


class FileSerializer(serializers.ModelSerializer):
    """
    Serializer which outputs user data.
    """

    class Meta:
        model = File
        fields = ['file_id', 'file_path']


class FileCreateSerializer(serializers.ModelSerializer):
    """
    Serializer which outputs user data.
    """

    class Meta:
        model = File
        fields = ['file_id', 'file_path']
        read_only_fields = ['file_id']
