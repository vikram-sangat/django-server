from django.contrib import admin
from .models import File


class FileAdmin(admin.ModelAdmin):
    list_display = ('file_id', 'file_path', 'user', 'created_at',)
    autocomplete_fields = ['user']


admin.site.register(File, FileAdmin)
