from rest_framework import serializers


class AccountActivationSerializer(serializers.Serializer):
    """
    Serializer which accepts an OAuth2 access token and provider.
    """
    activation_code = serializers.CharField(
        help_text='Enter activation code',
        max_length=4096, required=True, trim_whitespace=True)

    email = serializers.EmailField(required=True, trim_whitespace=True)
