from django.db import models
from datetime import datetime
from apps.core.funtions import generateUUID
from django.contrib.auth import get_user_model
# Create your models here.


class Referral(models.Model):
    referral_id = models.BigIntegerField(
        ('referal id'), default=generateUUID, primary_key=True, unique=True, editable=False)
    user = models.ForeignKey(
        get_user_model(), on_delete=models.SET_NULL, null=True, to_field='user_id', related_name='refferal_user')
    referral_code = models.CharField(max_length=20, db_index=True)
    created_at = models.DateTimeField(('created at'), default=datetime.now)
    updated_at = models.DateTimeField(
        ('updated at'), auto_now=True)

    def __str__(self):
        return f'{self.referral_id}'


class SocialMediaAccount(models.Model):
    account_id = models.BigIntegerField(
        ('account id'), default=generateUUID, primary_key=True, unique=True, editable=False)
    user = models.ForeignKey(
        get_user_model(), on_delete=models.SET_NULL, null=True,  to_field='user_id')
    platform = models.CharField(max_length=20, db_index=True, blank=True)
    platform_id = models.CharField(max_length=20, db_index=True, blank=True)
    platform_image = models.URLField(blank=True)
    created_at = models.DateTimeField(('created at'), default=datetime.now)
    updated_at = models.DateTimeField(
        ('updated at'), auto_now=True)


class AccountActivationCode(models.Model):
    activation_id = models.BigIntegerField(
        ('activation id'), default=generateUUID, primary_key=True, unique=True, editable=False)
    user = models.ForeignKey(
        get_user_model(), on_delete=models.SET_NULL, null=True, to_field='user_id')
    activation_code = models.BigIntegerField(
        ('activation code'), default=generateUUID, unique=True, editable=False)
    is_used = models.BooleanField('is code used', default=False)
    expires_at = models.DateTimeField('expires at')
    created_at = models.DateTimeField(('created at'), default=datetime.now)
    updated_at = models.DateTimeField(
        ('updated at'), auto_now=True)

    def __str__(self):
        return f"{self.activation_code}"
