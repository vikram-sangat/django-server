import json
import traceback
from datetime import datetime, timedelta

from django.contrib.auth import get_user_model
from django.db import transaction
from rest_framework import authentication, exceptions, permissions, status
from rest_framework.generics import (CreateAPIView, GenericAPIView,
                                     RetrieveAPIView)
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.account.models import AccountActivationCode, Referral

from .models import AccountActivationCode
from .serializers import AccountActivationSerializer


class ActivateUserAccount(GenericAPIView):

    serializer_class = AccountActivationSerializer
    permission_classes = [permissions.AllowAny]
    authentication_classes = ()

    def put(self, request, *args, **kwargs):
        """
        Update user
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.data

        try:
            activation = AccountActivationCode.objects.get(activation_code=data.get(
                'activation_code', ''), user__email=data.get('email', ''), is_used=False)
            activation.is_used = True
            activation.user.is_active = True
            activation.user.save()
            activation.save()
        except:
            traceback.print_exc()
            raise exceptions.ParseError("Activation code is invalid")

        return Response(status=status.HTTP_200_OK)
