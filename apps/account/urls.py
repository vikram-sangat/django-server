from django.urls import path
from . import views

urlpatterns = [
    path('activate/', views.ActivateUserAccount.as_view(), name='activate'),
]
