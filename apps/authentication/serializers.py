from rest_framework import serializers


class SocialSerializer(serializers.Serializer):
    """
    Serializer which accepts an OAuth2 access token and provider.
    """
    provider = serializers.ChoiceField(choices=['facebook', 'google'],)
    access_token = serializers.CharField(
        max_length=4096, required=True, trim_whitespace=True)


class UserNameSerializer(serializers.Serializer):
    """
    Serializer which accepts username and password.
    """
    email = serializers.EmailField(required=True, trim_whitespace=True)
    password = serializers.CharField(
        max_length=4096, required=True, trim_whitespace=True)
