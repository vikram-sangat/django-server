import json
from datetime import datetime, timedelta
import traceback

import facebook
import jwt
import requests
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from requests.exceptions import HTTPError
from rest_framework import authentication, exceptions, permissions, status
from rest_framework.authtoken.models import Token
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth import authenticate
from .serializers import SocialSerializer, UserNameSerializer
from .models import Session
from apps.core.funtions import generateUUID
from .utils import generateAccessToken


class Login(GenericAPIView):
    """
    Social Authentication
    """
    serializer_class = SocialSerializer
    authentication_classes = ()
    permission_classes = [permissions.AllowAny]

    def post(self, request, format=None):
        """Function for login and register
        :return:token for authorization or error
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        provider = serializer.data.get('provider', None)
        access_token = serializer.data.get(
            'access_token', '').replace('Bearer ', '')
        new_user = False
        User = get_user_model()
        picture = ''
        if provider == 'facebook':
            try:
                graph = facebook.GraphAPI(access_token=access_token)
                user_info = graph.get_object(
                    id='me',
                    fields='first_name, middle_name, last_name, id, '
                    'email, picture.type(large)')
                picture = user_info['picture']['data']['url']
            except facebook.GraphAPIError:
                traceback.print_exc()
                raise exceptions.AuthenticationFailed("Invalid access token.")
        if provider == 'google':
            payload = {'access_token': access_token}
            r = requests.get(
                'https://www.googleapis.com/oauth2/v2/userinfo', params=payload)

            if r.status_code != requests.codes.ok:
                raise exceptions.AuthenticationFailed("Invalid access token.")
            user_info = json.loads(r.text)
            picture = user_info.get('picture', '')

        if user_info:
            # User does not exists please register user.
            try:
                user = User.objects.get(email=user_info.get('email'))
            except User.DoesNotExist:
                new_user = True
                return JsonResponse({"new_user": True})

            # User does exists generate new token.
            try:
                tokenID = generateUUID()
                accessToken, refreshToken = generateAccessToken(user, tokenID)
                return JsonResponse({
                    'accessToken': accessToken,
                    'refreshToken': refreshToken,
                })
            except:
                traceback.print_exc()
                raise exceptions.AuthenticationFailed("Invalid access token.")


class LoginUser(GenericAPIView):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    serializer_class = UserNameSerializer
    permission_classes = [permissions.AllowAny]
    authentication_classes = ()

    def post(self, request, format=None):
        """Function for login and register
        :return:token for authorization or error
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        email = serializer.data.get('email', '')
        password = serializer.data.get('password', '')
        current_user = authenticate(
            email=email, password=password)
        if current_user is None:
            raise exceptions.AuthenticationFailed(
                'Invalid email and password.')
        if not current_user.is_active:
            raise exceptions.AuthenticationFailed('User is not active.')
        tokenID = generateUUID()
        accessToken, refreshToken = self._generateAccessToken(
            current_user, tokenID)
        return JsonResponse({
            'accessToken': accessToken,
            'refreshToken': refreshToken,
        })

    def _generateAccessToken(self, user, tokenID):
        try:
            return generateAccessToken(user, tokenID)
        except:
            traceback.print_exc()
            raise exceptions.AuthenticationFailed("Invalid request")


class LogoutUser(GenericAPIView):

    def delete(self, request, format=None):
        """
        Logout user
        """
        request.auth.delete()
        return Response(status=status.HTTP_200_OK)
