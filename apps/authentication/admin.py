from django.contrib import admin

from .models import Session


class SessionAdmin(admin.ModelAdmin):
    list_display = ('session_id', 'user', 'expire_at', 'created_at')


admin.site.register(Session, SessionAdmin)
