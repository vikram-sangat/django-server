from django.db import models
from datetime import datetime
from apps.core.funtions import generateUUID
from django.contrib.auth import get_user_model
# Create your models here.


class Session(models.Model):
    session_id = models.BigIntegerField(
        ('session id'), default=generateUUID, primary_key=True, unique=True, editable=False)
    access_token = models.TextField()
    refresh_token = models.TextField()
    user = models.ForeignKey(
        get_user_model(), on_delete=models.SET_NULL, null=True, to_field='user_id', default=None, blank=True)
    expire_at = models.DateTimeField(('expire at'))
    created_at = models.DateTimeField(('created at'), default=datetime.now)
    updated_at = models.DateTimeField(('updated at'), auto_now=True)

    def __str__(self):
        return f'{self.session_id}'
