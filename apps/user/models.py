from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import datetime
from django.db import models
from django.core import validators
from django.utils import timezone
from uuid import uuid1
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from rest_framework.authtoken.models import Token
from apps.core.funtions import generateUUID


class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class GenderChoice(models.TextChoices):
    MALE = 'male'
    FEMALE = 'female'
    OTHER = 'other'


class User(AbstractBaseUser, PermissionsMixin):
    """
    Custom user realization based on Django AbstractUser and PermissionMixin.
    """
    username = None
    email = models.EmailField(
        ('email address'),
        unique=True,
        error_messages={
            'unique': ("A user with that email already exists."),
        })
    user_id = models.BigIntegerField(
        ('user id'), default=generateUUID, primary_key=True, unique=True, editable=False)
    first_name = models.CharField(('first name'), max_length=255)
    last_name = models.CharField(('last name'), max_length=255)
    nickname = models.CharField(
        max_length=255, verbose_name=('nickname'), blank=True)
    address = models.TextField(('address'))
    country = models.CharField(('country'), max_length=100)
    state = models.CharField(('state'), max_length=100)
    city = models.CharField(('city'), max_length=100)
    pincode = models.IntegerField(('pincode'))
    phone = models.CharField(('phone'), max_length=20, unique=True)
    gender = models.CharField(
        ('gender'), max_length=10,  choices=GenderChoice.choices)
    is_staff = models.BooleanField(
        ('staff status'),
        default=False,
        help_text=('Designates whether the user can log into this admin '
                   'site.'))
    is_active = models.BooleanField(
        ('active'),
        default=True,
        help_text=('Designates whether this user should be treated as '
                   'active. Unselect this instead of deleting accounts.'))
    refferal = models.ForeignKey(
        to='account.Referral', to_field='referral_id', on_delete=models.SET_NULL, null=True, blank=True, related_name='refferal_user')
    date_of_birth = models.DateTimeField(
        ('date of birth'), blank=True, null=True)
    is_vendor = models.BooleanField(
        ('is vendor'),
        default=False,
        help_text=('Is vendor?'))
    created_at = models.DateTimeField(('created at'), default=datetime.now)
    updated_at = models.DateTimeField(
        ('updated at'), auto_now=True)
    objects = UserManager()
    # this is needed to use this model with django auth as a custom user class
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ["phone", "first_name", "last_name",
                       "address", "country", "state", "city", "pincode", "gender"]

    class Meta:
        managed = True
        abstract = False

    def __str__(self):
        return f'{self.email}'
