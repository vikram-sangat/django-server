from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User
from django.utils.translation import ugettext_lazy as _


class UsersAdmin(UserAdmin):

    fieldsets = (
        (None, {'fields': ('email', 'phone', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'gender',
                                         'nickname', 'country', 'state', 'city', 'address', 'pincode')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Refferd By'), {'fields': ('refferal',)}),
        (_('Important dates'), {'fields': ('date_of_birth', 'created_at',)}),
    )

    list_display = ('user_id', 'email', 'first_name', 'last_name',
                    'is_staff', 'is_active')
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)


admin.site.register(User, UsersAdmin)
