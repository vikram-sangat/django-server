import json
from datetime import datetime, timedelta
from rest_framework import authentication, permissions, status, exceptions
from rest_framework.generics import RetrieveAPIView, CreateAPIView, UpdateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth import get_user_model
from django.db import transaction
from drf_yasg.utils import swagger_auto_schema
from .models import User
from .serializers import UserSerializer, UserCreateSerializer
from apps.account.models import Referral, AccountActivationCode
import jwt
import traceback


class UserInfo(RetrieveAPIView):

    serializer_class = UserSerializer

    def retrieve(self, request, *args, **kwargs):
        """
        Retrive user information.
        """
        serializer = self.serializer_class(instance=request.user)
        return Response(serializer.data)


class UserRegister(CreateAPIView):

    serializer_class = UserCreateSerializer
    permission_classes = [permissions.AllowAny]
    authentication_classes = ()

    def create(self, request, *args, **kwargs):
        """
        Create user
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.data
        with transaction.atomic():
            today = datetime.today()
            activationCodeExpiry = today+timedelta(days=2)
            User = get_user_model()
            referral_code = data.pop('referral_code', '')
            print(data, referral_code)
            referance_user = None
            if referral_code is not None:
                try:
                    referance = Referral.objects.select_related('referred_by').get(
                        referral_code=referral_code)
                    referance_user = referance
                except:
                    traceback.print_exc()
                    raise exceptions.ParseError("Invalid referal code")

            newUser = User.objects.create_user(
                data.pop('email'),
                data.pop('password'),
                **data,
                is_active=False,
                refferd_by=referance_user
            )
            activationCode = AccountActivationCode(
                user=newUser,
                expires_at=activationCodeExpiry
            )
            activationCode.save()

        return Response(status=status.HTTP_200_OK)


class UpdateUser(UpdateAPIView):

    serializer_class = UserSerializer

    def update(self, request, *args, **kwargs):
        """
        Update user
        """
        serializer = self.serializer_class(request.user, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_200_OK)

    def partial_update(self, request, *args, **kwargs):
        """
        Partialy update user
        """
        serializer = self.serializer_class(
            request.user, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_200_OK)
