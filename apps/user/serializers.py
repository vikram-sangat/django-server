from django.contrib.auth import get_user_model
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    """
    Serializer which outputs user data.
    """
    class Meta:
        model = get_user_model()
        exclude = ['user_permissions', 'groups', 'updated_at',
                   'is_superuser', 'is_staff', 'created_at']
        extra_kwargs = {'password': {'write_only': True},
                        }

    def update(self, instance, validated_data):

        password = validated_data.pop('password', None)

        for (key, value) in validated_data.items():
            setattr(instance, key, value)

        if password is not None:
            instance.set_password(password)

        instance.save()

        return instance


class UserCreateSerializer(serializers.ModelSerializer):
    """
    Serializer which create user.
    """
    referral_code = serializers.CharField(
        allow_blank=True, allow_null=True, required=False)

    class Meta:
        model = get_user_model()
        exclude = ['user_permissions', 'groups', 'updated_at', "refferal",
                   'is_superuser', 'is_staff', 'created_at']
        read_only_fields = ['last_login', 'is_active', 'user_id']

        depth = 1
