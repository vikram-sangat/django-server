from uuid import uuid1


def generateUUID():
    return uuid1().time
