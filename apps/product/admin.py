from django.contrib import admin

from .models import Product


class ProductImagesInline(admin.TabularInline):
    model = Product.images.through


class ProductAdmin(admin.ModelAdmin):
    list_display = ('product_id', 'title', 'owner', 'ownership_since',
                    'category', 'sub_category', 'created_at')
    inlines = [
        ProductImagesInline,
    ]
    exclude = ['images']
    autocomplete_fields = ['owner', 'borrower', 'category', 'sub_category']


admin.site.register(Product, ProductAdmin)
