from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'', views.ProductViewMode, basename='products')

urlpatterns = [
    path('register/', views.ProductCreate.as_view(), name='product_register'),
    path('', include(router.urls), name='products')
]
