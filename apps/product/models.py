from django.db import models
from datetime import datetime
from apps.core.funtions import generateUUID
from django.contrib.auth import get_user_model
# Create your models here.


class OwnershipSince(models.TextChoices):
    ZERO_TO_POINTFIVE = 'ZERO_TO_POINTFIVE'
    POINTFIVE_TO_ONE = 'POINTFIVE_TO_ONE'
    ONE_TO_ONEPOINTFIVE = 'ONE_TO_ONEPOINTFIVE'
    ONEPOINTFIVE_TO_TWO = 'ONEPOINTFIVE_TO_TWO'
    TWO_TO_FIVE = 'TWO_TO_FIVE'
    FIVE_PLUS = 'FIVE_PLUS'
    UNKNOWN = 'UNKNOWN'


class ProductCondition(models.TextChoices):
    EXCELLENT = 'EXCELLENT'
    GOOD = 'GOOD'
    SATISFACTORY = 'SATISFACTORY'
    BAD = 'BAD'


class ImageType(models.TextChoices):
    IMAGE = 'IMAGE'
    VIDEO = 'VIDEO'


class ProductStatus(models.TextChoices):
    REVIEW = 'REVIEW'
    APPROVED = 'APPROVED'
    RENTED = 'RENTED'
    DELETED = 'DELETED'


class ProductPriceRange(models.TextChoices):
    ZERO_TO_FIVEHUNDRED = 'ZERO_TO_FIVEHUNDRED'
    FIVEHUNDRED_TO_ONETHOUSAND = 'FIVEHUNDRED_TO_ONETHOUSAND'
    ONETHOUSAND_TO_ONETHOUSANDFIVEHUNDRED = 'ONETHOUSAND_TO_ONETHOUSANDFIVEHUNDRED'
    ONETHOUSANDFIVEHUNDRED_TO_TWOTHOUSAND = 'ONETHOUSANDFIVEHUNDRED_TO_TWOTHOUSAND'
    TWOTHOUSAND_TO_TWOTHOUSANDFIVEHUNDRED = 'TWOTHOUSAND_TO_TWOTHOUSANDFIVEHUNDRED'
    TWOTHOUSANDFIVEHUNDRED_TO_THREETHOUSAND = 'TWOTHOUSANDFIVEHUNDRED_TO_THREETHOUSAND'
    THREETHOUSAND_TO_THREETHOUSANDFIVEHUNDRED = 'THREETHOUSAND_TO_THREETHOUSANDFIVEHUNDRED'
    THREETHOUSANDFIVEHUNDRED_TO_FOURTHOUSAND = 'THREETHOUSANDFIVEHUNDRED_TO_FOURTHOUSAND'
    FOURTHOUSAND_TO_FOURTHOUSANDFIVEHUNDRED = 'FOURTHOUSAND_TO_FOURTHOUSANDFIVEHUNDRED'
    FOURTHOUSANDFIVEHUNDRED_TO_FIVETHOUSAND = 'FOURTHOUSANDFIVEHUNDRED_TO_FIVETHOUSAND'
    FIVETHOUSAND_TO_TENTHOUSAND = 'FIVETHOUSAND_TO_TENTHOUSAND'
    TENTHOUSAND_TO_THIRTYTHOUSANDPLUS = 'TENTHOUSAND_TO_THIRTYTHOUSANDPLUS'
    UNKNOWN = 'UNKNOWN'


class Product(models.Model):
    product_id = models.BigIntegerField(
        ('product id'), default=generateUUID, primary_key=True, unique=True, editable=False)
    title = models.CharField('product name', max_length=255, db_index=True)
    description = models.TextField('product description')
    manufacturer = models.CharField(
        'manufacturor', max_length=255, db_index=True)
    ownership_since = models.CharField(
        ('Ownership since'), choices=OwnershipSince.choices, max_length=255,  default=OwnershipSince.UNKNOWN, db_index=True)
    condition = models.CharField(
        'condition', choices=ProductCondition.choices, max_length=255, db_index=True, default=ProductCondition.SATISFACTORY)
    images = models.ManyToManyField(
        'files.File', through='ProductImages', related_name='images')
    price_range = models.CharField(
        'price range', choices=ProductPriceRange.choices, max_length=255, db_index=True, default=ProductPriceRange.UNKNOWN)
    owner = models.ForeignKey(
        get_user_model(), on_delete=models.DO_NOTHING, to_field='user_id', related_name='owner')
    borrower = models.ForeignKey(
        get_user_model(), on_delete=models.SET_NULL, to_field='user_id', null=True, blank=True, related_name='borrower')
    category = models.ForeignKey(
        to='categories.Category', on_delete=models.DO_NOTHING, to_field='category_id')
    sub_category = models.ForeignKey(
        to='categories.Category', on_delete=models.SET_NULL, to_field='category_id', related_name='sub_category', null=True, blank=True)
    rent_weekly = models.PositiveBigIntegerField(
        ('rent weekly'), blank=True, null=True)
    rent_monthly = models.PositiveBigIntegerField(
        ('rent monthly'), blank=True, null=True)
    rent_yearly = models.PositiveBigIntegerField(
        ('rent yearly'), blank=True, null=True)
    rent_weekly_suggested = models.PositiveBigIntegerField(
        ('rent weekly suggested'), blank=True, null=True)
    rent_monthly_suggested = models.PositiveBigIntegerField(
        ('rent monthly suggested'), blank=True, null=True)
    rent_yearly_suggested = models.PositiveBigIntegerField(
        ('rent yearly suggested '), blank=True, null=True)
    status = models.CharField(
        ('product status'), choices=ProductStatus.choices, max_length=255,  default=ProductStatus.REVIEW, db_index=True)
    status_change_date = models.DateTimeField(
        ('status change date'), default=datetime.now)
    created_at = models.DateTimeField(('created at'), default=datetime.now)
    updated_at = models.DateTimeField(('updated at'), auto_now=True)

    def __str__(self):
        return f'{self.product_id}'


class ProductImages(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    file = models.ForeignKey('files.File', on_delete=models.CASCADE)
    description = models.TextField('File description')
    type = models.CharField(
        ('file type'), choices=ImageType.choices, max_length=255,  default=ImageType.IMAGE, db_index=True)
    created_at = models.DateTimeField(('created at'), default=datetime.now)
    updated_at = models.DateTimeField(('updated at'), auto_now=True)

    def __str__(self):
        return f'{self.type}'
