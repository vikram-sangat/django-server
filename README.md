## HOW to run server

1. Local
   python manage.py runserver --settings=mysite.settings --configuration=Dev
2. Gunicorn
   gunicorn server.wsgi
